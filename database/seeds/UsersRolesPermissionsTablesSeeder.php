<?php

use Illuminate\Database\Seeder;

class UsersRolesPermissionsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'first_name' => 'Admin',
            'last_name' => 'Administrator',
            'email'=> 'admissions@cloudhorizon.com',
            'email_verified_at' => \Carbon\Carbon::now(),
            'phone_number' => '+381112233456',
            'password' => \Illuminate\Support\Facades\Hash::make('123456')
            ]);

        $role = \Spatie\Permission\Models\Role::create(['name' => 'Administrator', 'guard_name' => 'web']);

        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'Administer roles & permissions', 'guard_name' => 'web']);

        $role->givePermissionTo($permission);

        $user->assignRole($role);
    }
}
