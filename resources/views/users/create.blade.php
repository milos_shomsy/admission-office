{{-- \resources\views\users\create.blade.php --}}
@extends('layouts.app')

@section('title', '| Add User')

@section('content')
    <div class="container">
        <div class='col-lg-12 col-lg-offset-12'>

            <h1><i class='fa fa-user-plus'></i> Add User</h1>
            <hr>

            {{ Form::open(array('url' => 'users')) }}

            {!! BootForm::text('name')  !!}

            {!! BootForm::text('email')  !!}

            <div class='form-group'>
                @foreach ($roles as $role)
                    {{ Form::checkbox('roles[]',  $role->id ) }}
                    {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                @endforeach
            </div>

            {!! BootForm::password() !!}

            <div class="form-group">
                {{ Form::label('password', 'Confirm Password') }}<br>
                {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
            </div>

            {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}

        </div>
    </div>

@endsection