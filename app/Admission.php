<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admission extends Model
{
    protected $fillable = ['name','description','start_date','start_time', 'status', 'type'];

}
