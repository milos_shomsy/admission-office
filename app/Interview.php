<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Latfur\Event\Models\Event;
use App\User;

class Interview extends Event
{
    protected $table = 'events';
    protected $fillable = ['event_title',
        'event_start_date',
        'event_start_time',
        'event_description',
        'event_end_time',
        'event_end_date',
        'user_id'];

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
