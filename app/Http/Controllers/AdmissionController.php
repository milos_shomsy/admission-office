<?php

namespace App\Http\Controllers;

use App\Admission;
use App\Interview;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Latfur\Event\Http\Controllers\EventController;
use Latfur\Event\Models\Event;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class AdmissionController extends EventController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('event.calendar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Interview::create([
            'event_title' => $request->event_title,
            'event_start_date' => Carbon::parse($request->event_start_date)->format('Y-m-d'),
            'event_start_time' => $request->event_start_time,
            'event_description' => $request->event_description,
            'event_end_time' => $request->event_start_time,
            'event_end_date' => Carbon::parse($request->event_start_date)->format('Y-m-d'),
            'user_id' => Auth::user()->id
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function cancelInterview(Request $request, $id)
    {
        dd($request->all());
    }

    public function all_event(){

        $all_event = Auth::user()->hasRole('Administrator') ? Event::all()->toArray() : Event::where('user_id', Auth::user()->id)->toArray();

        $event_data=array();
        foreach ($all_event as $key => $event_val) {
            $event_data[$key]['title'] =$event_val['event_title'];
            $event_data[$key]['start'] =$event_val['event_start_date'].' '.date('H:i:s', strtotime($event_val['event_start_time']));
            $event_data[$key]['end']  =$event_val['event_end_date'].' '.date('H:i:s', strtotime($event_val['event_end_time']));

            $event_data[$key]['start_formate'] =implode("/", array_reverse(explode("-", $event_val['event_start_date']))).' '.date('h:i:s A', strtotime($event_val['event_start_time']));
            $event_data[$key]['end_formate']  =implode("/", array_reverse(explode("-", $event_val['event_end_date']))).' '.date('h:i:s A', strtotime($event_val['event_end_time']));


            $event_data[$key]['events_id'] = $event_val['id'];
            $event_data[$key]['event_description'] =$event_val['event_description'];
            $event_data[$key]['created_at'] =date('d/m/Y', strtotime($event_val['created_at']));

        }

        echo json_encode($event_data);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
